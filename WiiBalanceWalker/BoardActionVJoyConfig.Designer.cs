﻿namespace WiiBalanceWalker
{
    partial class BoardActionVJoyConfig
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblX = new System.Windows.Forms.Label();
            this.lblY = new System.Windows.Forms.Label();
            this.btnSetCenterOffset = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblX
            // 
            this.lblX.AutoSize = true;
            this.lblX.Location = new System.Drawing.Point(0, 0);
            this.lblX.Name = "lblX";
            this.lblX.Size = new System.Drawing.Size(62, 13);
            this.lblX.TabIndex = 0;
            this.lblX.Text = "VJoy config";
            // 
            // lblY
            // 
            this.lblY.AutoSize = true;
            this.lblY.Location = new System.Drawing.Point(3, 13);
            this.lblY.Name = "lblY";
            this.lblY.Size = new System.Drawing.Size(35, 13);
            this.lblY.TabIndex = 1;
            this.lblY.Text = "label2";
            // 
            // btnSetCenterOffset
            // 
            this.btnSetCenterOffset.Location = new System.Drawing.Point(6, 29);
            this.btnSetCenterOffset.Name = "btnSetCenterOffset";
            this.btnSetCenterOffset.Size = new System.Drawing.Size(120, 23);
            this.btnSetCenterOffset.TabIndex = 2;
            this.btnSetCenterOffset.Text = "Set Center Offset";
            this.btnSetCenterOffset.UseVisualStyleBackColor = true;
            this.btnSetCenterOffset.Click += new System.EventHandler(this.btnSetCenterOffset_Click);
            // 
            // BoardActionVJoyConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnSetCenterOffset);
            this.Controls.Add(this.lblY);
            this.Controls.Add(this.lblX);
            this.Name = "BoardActionVJoyConfig";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblX;
        private System.Windows.Forms.Label lblY;
        private System.Windows.Forms.Button btnSetCenterOffset;
    }
}
