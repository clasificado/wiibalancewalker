﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using vJoyInterfaceWrap;

namespace WiiBalanceWalker
{
    class BoardActionVJoy : IBoardAction
    {
        public string ConfigControlTitle
        {
            get { return "VJoy"; }
        }

        public string ConfigControlTypeName
        {
            get { return "WiiBalanceWalker.BoardActionVJoyConfig"; }
        }

        public vJoy joystick;
        public vJoy.JoystickState iReport;
        public uint id = 1;

        int nButtons;
        int ContPovNumber;
        int DiscPovNumber;

        long maxval = 0;
        bool setCenterOffset = false;
        float naCorners = 0f;

        float oaTopLeft = 0f;
        float oaTopRight = 0f;
        float oaBottomLeft = 0f;
        float oaBottomRight = 0f;

        public System.Windows.Forms.Control ConfigControl
        {
            get;
            set;
        }
        public void Init()
        {
            joystick = new vJoy();
            iReport = new vJoy.JoystickState();


            // Device ID can only be in the range 1-16
            if (id <= 0 || id > 16)
            {
                WriteLine("Illegal device ID {0}\nExit!", id);
                return;
            }

            // Get the driver attributes (Vendor ID, Product ID, Version Number)
            if (!joystick.vJoyEnabled())
            {
                WriteLine("vJoy driver not enabled: Failed Getting vJoy attributes.\n");
            }
            //else
                //WriteLine("Vendor: {0}\nProduct :{1}\nVersion Number:{2}\n", joystick.GetvJoyManufacturerString(), joystick.GetvJoyProductString(), joystick.GetvJoySerialNumberString());

            // Get the state of the requested device
            VjdStat status = joystick.GetVJDStatus(id);
            switch (status)
            {
                case VjdStat.VJD_STAT_OWN:
                    WriteLine("vJoy Device {0} is already owned by this feeder\n", id);
                    break;
                case VjdStat.VJD_STAT_FREE:
                    //WriteLine("vJoy Device {0} is free\n", id);
                    break;
                case VjdStat.VJD_STAT_BUSY:
                    WriteLine("vJoy Device {0} is already owned by another feeder\nCannot continue\n", id);
                    return;
                case VjdStat.VJD_STAT_MISS:
                    WriteLine("vJoy Device {0} is not installed or disabled\nCannot continue\n", id);
                    return;
                default:
                    WriteLine("vJoy Device {0} general error\nCannot continue\n", id);
                    return;
            };

            // Check which axes are supported
            bool AxisX = joystick.GetVJDAxisExist(id, HID_USAGES.HID_USAGE_X);
            bool AxisY = joystick.GetVJDAxisExist(id, HID_USAGES.HID_USAGE_Y);
            bool AxisZ = joystick.GetVJDAxisExist(id, HID_USAGES.HID_USAGE_Z);
            bool AxisRX = joystick.GetVJDAxisExist(id, HID_USAGES.HID_USAGE_RX);
            bool AxisRZ = joystick.GetVJDAxisExist(id, HID_USAGES.HID_USAGE_RZ);
            // Get the number of buttons and POV Hat switchessupported by this vJoy device
            nButtons = joystick.GetVJDButtonNumber(id);
            ContPovNumber = joystick.GetVJDContPovNumber(id);
            DiscPovNumber = joystick.GetVJDDiscPovNumber(id);

            // Print results
            /*WriteLine("\nvJoy Device {0} capabilities:\n", id);
            WriteLine("Numner of buttons\t\t{0}\n", nButtons);
            WriteLine("Numner of Continuous POVs\t{0}\n", ContPovNumber);
            WriteLine("Numner of Descrete POVs\t\t{0}\n", DiscPovNumber);
            WriteLine("Axis X\t\t{0}\n", AxisX ? "Yes" : "No");
            WriteLine("Axis Y\t\t{0}\n", AxisX ? "Yes" : "No");
            WriteLine("Axis Z\t\t{0}\n", AxisX ? "Yes" : "No");
            WriteLine("Axis Rx\t\t{0}\n", AxisRX ? "Yes" : "No");
            WriteLine("Axis Rz\t\t{0}\n", AxisRZ ? "Yes" : "No");
             * */

            // Test if DLL matches the driver
            UInt32 DllVer = 0, DrvVer = 0;
            bool match = joystick.DriverMatch(ref DllVer, ref DrvVer);
            if (!match)
            {
                WriteLine("Version of Driver ({0:X}) does NOT match DLL Version ({1:X})\n", DrvVer, DllVer);
            }

            // Acquire the target
            if ((status == VjdStat.VJD_STAT_OWN) || ((status == VjdStat.VJD_STAT_FREE) && (!joystick.AcquireVJD(id))))
            {
                WriteLine("Failed to acquire vJoy device number {0}.\n", id);
            }

            joystick.GetVJDAxisMax(id, HID_USAGES.HID_USAGE_X, ref maxval);

            // Reset this device to default values
            joystick.ResetVJD(id);
        }

        private void WriteLine(params object[] o)
        {
            var Status = this.GetType().Name + ": ";
            Status += String.Join(", ", o);
            throw new ApplicationException(Status);
        }

        public BalanceBoardManager.TickData ActionTick(BalanceBoardManager.TickData send)
        {
            var wiimoteState = send.WiimoteState;
            bool res;

            var rwWeight = wiimoteState.BalanceBoardState.WeightKg;
            var rwTopLeft = wiimoteState.BalanceBoardState.SensorValuesKg.TopLeft;
            var rwTopRight = wiimoteState.BalanceBoardState.SensorValuesKg.TopRight;
            var rwBottomLeft = wiimoteState.BalanceBoardState.SensorValuesKg.BottomLeft;
            var rwBottomRight = wiimoteState.BalanceBoardState.SensorValuesKg.BottomRight;

            // Get offset that would make current values the center of mass.
            var btnSetCenterOffset = (Button)ConfigControl.Controls.Find("btnSetCenterOffset", true)[0];
            setCenterOffset = !(btnSetCenterOffset).Enabled;
            if (setCenterOffset)
            {
                setCenterOffset = false;
                btnSetCenterOffset.Enabled = true;

                var rwHighest = Math.Max(Math.Max(rwTopLeft, rwTopRight), Math.Max(rwBottomLeft, rwBottomRight));

                oaTopLeft = rwHighest - rwTopLeft;
                oaTopRight = rwHighest - rwTopRight;
                oaBottomLeft = rwHighest - rwBottomLeft;
                oaBottomRight = rwHighest - rwBottomRight;
            }

            // Prevent negative values by tracking lowest possible value and making it a zero based offset.
            if (rwTopLeft < naCorners) naCorners = rwTopLeft;
            if (rwTopRight < naCorners) naCorners = rwTopRight;
            if (rwBottomLeft < naCorners) naCorners = rwBottomLeft;
            if (rwBottomRight < naCorners) naCorners = rwBottomRight;

            // Negative total weight is reset to zero as jumping or lifting the board causes negative spikes, which would break 'in use' checks.
            var owWeight = rwWeight < 0f ? 0f : rwWeight;
            var owTopLeft = rwTopLeft -= naCorners;
            var owTopRight = rwTopRight -= naCorners;
            var owBottomLeft = rwBottomLeft -= naCorners;
            var owBottomRight = rwBottomRight -= naCorners;

            // Keep values only when board is being used, otherwise offsets and small value jitters can trigger unwanted actions.
            if (owWeight > 0f)
            {
                owTopLeft += oaTopLeft;
                owTopRight += oaTopRight;
                owBottomLeft += oaBottomLeft;
                owBottomRight += oaBottomRight;
            }
            else
            {
                owTopLeft = 0;
                owTopRight = 0;
                owBottomLeft = 0;
                owBottomRight = 0;
            }


            // Calculate each weight ratio.

            var owrPercentage = 100 / (owTopLeft + owTopRight + owBottomLeft + owBottomRight);
            var owrTopLeft = owrPercentage * owTopLeft;
            var owrTopRight = owrPercentage * owTopRight;
            var owrBottomLeft = owrPercentage * owBottomLeft;
            var owrBottomRight = owrPercentage * owBottomRight;

            var brX = owrBottomRight + owrTopRight; //50 center
            var brY = owrBottomRight + owrBottomLeft;  //50 center

            var vjoyX = 32768 * brX / 100;
            var vjoyY = 32768 * brY / 100;


            ((Label)ConfigControl.Controls.Find("lblX", true)[0]).Text = vjoyX.ToString();
            ((Label)ConfigControl.Controls.Find("lblY", true)[0]).Text = vjoyY.ToString();

            joystick.SetAxis(Convert.ToInt32(vjoyX), id, HID_USAGES.HID_USAGE_X);
            joystick.SetAxis(Convert.ToInt32(vjoyY), id, HID_USAGES.HID_USAGE_Y);

            return send;
        }

        public void Stop()
        {
        }
    }
}