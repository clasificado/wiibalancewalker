﻿using System;
using System.Windows.Forms;
using InTheHand.Net.Sockets;
using InTheHand.Net.Bluetooth;
using WiimoteLib;

namespace WiiBalanceWalker
{
    public partial class FormBluetooth : Form
    {
        BluetoothManager Bluetooth = new BluetoothManager();
        public FormBluetooth()
        {
            InitializeComponent();
        }

        private void button_DeviceSearch_Click(object sender, EventArgs e)
        {
            ((Button)sender).Enabled = false;

            try
            {
                Bluetooth.Search();
            }
            catch (Exception ex)
            {
                label_Status.Text = "Error: " + ex.Message;
            }

            ((Button)sender).Enabled = true;
        }

        private void FormBluetooth_Load(object sender, EventArgs e)
        {
            Bluetooth.StatusUpdated += Bluetooth_StatusUpdated;
        }

        void Bluetooth_StatusUpdated(string obj)
        {
            label_Status.Text = obj;
            label_Status.Refresh();
        }

        private void checkBox_RemoveExisting_CheckedChanged(object sender, EventArgs e)
        {
            bool Out = ((CheckBox)sender).Checked;
            Bluetooth.RemoveExisting = Out;
        }

        private void checkBox_PermanentSync_CheckedChanged(object sender, EventArgs e)
        {
            bool Out = ((CheckBox)sender).Checked;
            Bluetooth.PermanentSync = Out;
        }

        private void checkBox_SkipNameCheck_CheckedChanged(object sender, EventArgs e)
        {
            bool Out = ((CheckBox)sender).Checked;
            Bluetooth.SkipNameCheck = Out;
        }

        
    }
}
