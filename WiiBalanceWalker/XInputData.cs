﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WiiBalanceWalker
{
    class XInputData
    {
        //https://msdn.microsoft.com/en-us/library/windows/desktop/microsoft.directx_sdk.reference.xinput_gamepad(v=vs.85).aspx
        public enum GamepadEnum
        {
            XINPUT_GAMEPAD_DPAD_UP = 0x0001,
            XINPUT_GAMEPAD_DPAD_DOWN = 0x0002,
            XINPUT_GAMEPAD_DPAD_LEFT = 0x0004,
            XINPUT_GAMEPAD_DPAD_RIGHT = 0x0008,
            XINPUT_GAMEPAD_START = 0x0010,
            XINPUT_GAMEPAD_BACK = 0x0020,
            XINPUT_GAMEPAD_LEFT_THUMB = 0x0040,
            XINPUT_GAMEPAD_RIGHT_THUMB = 0x0080,
            XINPUT_GAMEPAD_LEFT_SHOULDER = 0x0100,
            XINPUT_GAMEPAD_RIGHT_SHOULDER = 0x0200,
            XINPUT_GAMEPAD_A = 0x1000,
            XINPUT_GAMEPAD_B = 0x2000,
            XINPUT_GAMEPAD_X = 0x4000,
            XINPUT_GAMEPAD_Y = 0x8000
        }
        public GamepadEnum GamepadData;
        byte LeftTriger;
        byte RightTrigger;
        short ThumbLX;
        short ThumbLY;
        short ThumbRX;
        short ThumbRY;
    }
}
