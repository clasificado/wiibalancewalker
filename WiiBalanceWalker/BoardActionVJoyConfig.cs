﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WiiBalanceWalker
{
    public partial class BoardActionVJoyConfig : UserControl
    {
        public BoardActionVJoyConfig()
        {
            InitializeComponent();
        }

        private void btnSetCenterOffset_Click(object sender, EventArgs e)
        {
            ((Button)sender).Enabled = false;
        }
    }
}
