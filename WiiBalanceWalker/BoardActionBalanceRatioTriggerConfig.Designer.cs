﻿namespace WiiBalanceWalker
{
    partial class BoardActionBalanceRatioTriggerConfig
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.numericUpDown_TMFB = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_TMLR = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_TFB = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_TLR = new System.Windows.Forms.NumericUpDown();
            this.label_TMFB = new System.Windows.Forms.Label();
            this.label_TMLR = new System.Windows.Forms.Label();
            this.label_TFB = new System.Windows.Forms.Label();
            this.label_TLR = new System.Windows.Forms.Label();
            this.button_SetCenterOffset = new System.Windows.Forms.Button();
            this.lblCurrentActionText = new System.Windows.Forms.Label();
            this.lblCurrentAction = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_TMFB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_TMLR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_TFB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_TLR)).BeginInit();
            this.SuspendLayout();
            // 
            // numericUpDown_TMFB
            // 
            this.numericUpDown_TMFB.Location = new System.Drawing.Point(175, 78);
            this.numericUpDown_TMFB.Maximum = new decimal(new int[] {
            51,
            0,
            0,
            0});
            this.numericUpDown_TMFB.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_TMFB.Name = "numericUpDown_TMFB";
            this.numericUpDown_TMFB.Size = new System.Drawing.Size(49, 20);
            this.numericUpDown_TMFB.TabIndex = 19;
            this.numericUpDown_TMFB.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_TMFB.ValueChanged += new System.EventHandler(this.numericUpDown_TMFB_ValueChanged);
            // 
            // numericUpDown_TMLR
            // 
            this.numericUpDown_TMLR.Location = new System.Drawing.Point(175, 52);
            this.numericUpDown_TMLR.Maximum = new decimal(new int[] {
            51,
            0,
            0,
            0});
            this.numericUpDown_TMLR.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_TMLR.Name = "numericUpDown_TMLR";
            this.numericUpDown_TMLR.Size = new System.Drawing.Size(49, 20);
            this.numericUpDown_TMLR.TabIndex = 18;
            this.numericUpDown_TMLR.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_TMLR.ValueChanged += new System.EventHandler(this.numericUpDown_TMLR_ValueChanged);
            // 
            // numericUpDown_TFB
            // 
            this.numericUpDown_TFB.Location = new System.Drawing.Point(175, 26);
            this.numericUpDown_TFB.Maximum = new decimal(new int[] {
            51,
            0,
            0,
            0});
            this.numericUpDown_TFB.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_TFB.Name = "numericUpDown_TFB";
            this.numericUpDown_TFB.Size = new System.Drawing.Size(49, 20);
            this.numericUpDown_TFB.TabIndex = 17;
            this.numericUpDown_TFB.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_TFB.ValueChanged += new System.EventHandler(this.numericUpDown_TFB_ValueChanged);
            // 
            // numericUpDown_TLR
            // 
            this.numericUpDown_TLR.Location = new System.Drawing.Point(175, 0);
            this.numericUpDown_TLR.Maximum = new decimal(new int[] {
            51,
            0,
            0,
            0});
            this.numericUpDown_TLR.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_TLR.Name = "numericUpDown_TLR";
            this.numericUpDown_TLR.Size = new System.Drawing.Size(49, 20);
            this.numericUpDown_TLR.TabIndex = 12;
            this.numericUpDown_TLR.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_TLR.ValueChanged += new System.EventHandler(this.numericUpDown_TLR_ValueChanged);
            // 
            // label_TMFB
            // 
            this.label_TMFB.AutoSize = true;
            this.label_TMFB.Location = new System.Drawing.Point(3, 80);
            this.label_TMFB.Name = "label_TMFB";
            this.label_TMFB.Size = new System.Drawing.Size(156, 13);
            this.label_TMFB.TabIndex = 13;
            this.label_TMFB.Text = "- Modifier + Foward / Backward";
            // 
            // label_TMLR
            // 
            this.label_TMLR.AutoSize = true;
            this.label_TMLR.Location = new System.Drawing.Point(3, 54);
            this.label_TMLR.Name = "label_TMLR";
            this.label_TMLR.Size = new System.Drawing.Size(116, 13);
            this.label_TMLR.TabIndex = 14;
            this.label_TMLR.Text = "- Modifier + Left / Right";
            // 
            // label_TFB
            // 
            this.label_TFB.AutoSize = true;
            this.label_TFB.Location = new System.Drawing.Point(3, 28);
            this.label_TFB.Name = "label_TFB";
            this.label_TFB.Size = new System.Drawing.Size(110, 13);
            this.label_TFB.TabIndex = 15;
            this.label_TFB.Text = "- Forward / Backward";
            // 
            // label_TLR
            // 
            this.label_TLR.AutoSize = true;
            this.label_TLR.Location = new System.Drawing.Point(3, 2);
            this.label_TLR.Name = "label_TLR";
            this.label_TLR.Size = new System.Drawing.Size(67, 13);
            this.label_TLR.TabIndex = 16;
            this.label_TLR.Text = "- Left / Right";
            // 
            // button_SetCenterOffset
            // 
            this.button_SetCenterOffset.Location = new System.Drawing.Point(6, 112);
            this.button_SetCenterOffset.Name = "button_SetCenterOffset";
            this.button_SetCenterOffset.Size = new System.Drawing.Size(138, 28);
            this.button_SetCenterOffset.TabIndex = 20;
            this.button_SetCenterOffset.Text = "Set balance as center";
            this.button_SetCenterOffset.UseVisualStyleBackColor = true;
            this.button_SetCenterOffset.Click += new System.EventHandler(this.button_SetCenterOffset_Click);
            // 
            // lblCurrentActionText
            // 
            this.lblCurrentActionText.AutoSize = true;
            this.lblCurrentActionText.Location = new System.Drawing.Point(3, 143);
            this.lblCurrentActionText.Name = "lblCurrentActionText";
            this.lblCurrentActionText.Size = new System.Drawing.Size(77, 13);
            this.lblCurrentActionText.TabIndex = 21;
            this.lblCurrentActionText.Text = "Current Action:";
            // 
            // lblCurrentAction
            // 
            this.lblCurrentAction.AutoSize = true;
            this.lblCurrentAction.Location = new System.Drawing.Point(86, 143);
            this.lblCurrentAction.Name = "lblCurrentAction";
            this.lblCurrentAction.Size = new System.Drawing.Size(45, 13);
            this.lblCurrentAction.TabIndex = 22;
            this.lblCurrentAction.Text = "-not set-";
            // 
            // BoardActionBalanceRatioTriggerConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblCurrentAction);
            this.Controls.Add(this.lblCurrentActionText);
            this.Controls.Add(this.button_SetCenterOffset);
            this.Controls.Add(this.numericUpDown_TMFB);
            this.Controls.Add(this.numericUpDown_TMLR);
            this.Controls.Add(this.numericUpDown_TFB);
            this.Controls.Add(this.numericUpDown_TLR);
            this.Controls.Add(this.label_TMFB);
            this.Controls.Add(this.label_TMLR);
            this.Controls.Add(this.label_TFB);
            this.Controls.Add(this.label_TLR);
            this.Name = "BoardActionBalanceRatioTriggerConfig";
            this.Size = new System.Drawing.Size(225, 172);
            this.Load += new System.EventHandler(this.BoardActionBalanceRatioTriggerConfig_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_TMFB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_TMLR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_TFB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_TLR)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown numericUpDown_TMFB;
        private System.Windows.Forms.NumericUpDown numericUpDown_TMLR;
        private System.Windows.Forms.NumericUpDown numericUpDown_TFB;
        private System.Windows.Forms.NumericUpDown numericUpDown_TLR;
        private System.Windows.Forms.Label label_TMFB;
        private System.Windows.Forms.Label label_TMLR;
        private System.Windows.Forms.Label label_TFB;
        private System.Windows.Forms.Label label_TLR;
        private System.Windows.Forms.Button button_SetCenterOffset;
        private System.Windows.Forms.Label lblCurrentActionText;
        private System.Windows.Forms.Label lblCurrentAction;
    }
}
