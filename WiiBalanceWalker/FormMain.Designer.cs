﻿namespace WiiBalanceWalker
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_rwWT = new System.Windows.Forms.Label();
            this.button_Connect = new System.Windows.Forms.Button();
            this.label_brX = new System.Windows.Forms.Label();
            this.label_brY = new System.Windows.Forms.Label();
            this.label_brDL = new System.Windows.Forms.Label();
            this.label_brDR = new System.Windows.Forms.Label();
            this.groupBox_RawWeight = new System.Windows.Forms.GroupBox();
            this.label_rwBR = new System.Windows.Forms.Label();
            this.label_rwBL = new System.Windows.Forms.Label();
            this.label_rwTR = new System.Windows.Forms.Label();
            this.label_rwTL = new System.Windows.Forms.Label();
            this.groupBox_OffsetWeight = new System.Windows.Forms.GroupBox();
            this.label_owWT = new System.Windows.Forms.Label();
            this.label_owTL = new System.Windows.Forms.Label();
            this.label_owTR = new System.Windows.Forms.Label();
            this.label_owBL = new System.Windows.Forms.Label();
            this.label_owBR = new System.Windows.Forms.Label();
            this.groupBox_OffsetWeightRatio = new System.Windows.Forms.GroupBox();
            this.label_owrTL = new System.Windows.Forms.Label();
            this.label_owrTR = new System.Windows.Forms.Label();
            this.label_owrBL = new System.Windows.Forms.Label();
            this.label_owrBR = new System.Windows.Forms.Label();
            this.groupBox_General = new System.Windows.Forms.GroupBox();
            this.button_ResetDefaults = new System.Windows.Forms.Button();
            this.button_BluetoothAddDevice = new System.Windows.Forms.Button();
            this.groupBox_BalanceRatio = new System.Windows.Forms.GroupBox();
            this.label_brDF = new System.Windows.Forms.Label();
            this.label_Status = new System.Windows.Forms.Label();
            this.groupBox_Actions = new System.Windows.Forms.GroupBox();
            this.tcAction = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.checkBox_DisableActions = new System.Windows.Forms.CheckBox();
            this.groupBox_RawWeight.SuspendLayout();
            this.groupBox_OffsetWeight.SuspendLayout();
            this.groupBox_OffsetWeightRatio.SuspendLayout();
            this.groupBox_General.SuspendLayout();
            this.groupBox_BalanceRatio.SuspendLayout();
            this.groupBox_Actions.SuspendLayout();
            this.tcAction.SuspendLayout();
            this.SuspendLayout();
            // 
            // label_rwWT
            // 
            this.label_rwWT.AutoSize = true;
            this.label_rwWT.Location = new System.Drawing.Point(63, 113);
            this.label_rwWT.Name = "label_rwWT";
            this.label_rwWT.Size = new System.Drawing.Size(25, 13);
            this.label_rwWT.TabIndex = 0;
            this.label_rwWT.Text = "WT";
            // 
            // button_Connect
            // 
            this.button_Connect.Location = new System.Drawing.Point(6, 19);
            this.button_Connect.Name = "button_Connect";
            this.button_Connect.Size = new System.Drawing.Size(183, 48);
            this.button_Connect.TabIndex = 0;
            this.button_Connect.Text = "Connect to Wii balance board";
            this.button_Connect.UseVisualStyleBackColor = true;
            this.button_Connect.Click += new System.EventHandler(this.button_Connect_Click);
            // 
            // label_brX
            // 
            this.label_brX.AutoSize = true;
            this.label_brX.Location = new System.Drawing.Point(25, 32);
            this.label_brX.Name = "label_brX";
            this.label_brX.Size = new System.Drawing.Size(14, 13);
            this.label_brX.TabIndex = 0;
            this.label_brX.Text = "X";
            // 
            // label_brY
            // 
            this.label_brY.AutoSize = true;
            this.label_brY.Location = new System.Drawing.Point(101, 32);
            this.label_brY.Name = "label_brY";
            this.label_brY.Size = new System.Drawing.Size(14, 13);
            this.label_brY.TabIndex = 0;
            this.label_brY.Text = "Y";
            // 
            // label_brDL
            // 
            this.label_brDL.AutoSize = true;
            this.label_brDL.Location = new System.Drawing.Point(25, 76);
            this.label_brDL.Name = "label_brDL";
            this.label_brDL.Size = new System.Drawing.Size(21, 13);
            this.label_brDL.TabIndex = 0;
            this.label_brDL.Text = "DL";
            // 
            // label_brDR
            // 
            this.label_brDR.AutoSize = true;
            this.label_brDR.Location = new System.Drawing.Point(101, 76);
            this.label_brDR.Name = "label_brDR";
            this.label_brDR.Size = new System.Drawing.Size(23, 13);
            this.label_brDR.TabIndex = 0;
            this.label_brDR.Text = "DR";
            // 
            // groupBox_RawWeight
            // 
            this.groupBox_RawWeight.Controls.Add(this.label_rwBR);
            this.groupBox_RawWeight.Controls.Add(this.label_rwBL);
            this.groupBox_RawWeight.Controls.Add(this.label_rwTR);
            this.groupBox_RawWeight.Controls.Add(this.label_rwTL);
            this.groupBox_RawWeight.Controls.Add(this.label_rwWT);
            this.groupBox_RawWeight.Location = new System.Drawing.Point(12, 12);
            this.groupBox_RawWeight.Name = "groupBox_RawWeight";
            this.groupBox_RawWeight.Size = new System.Drawing.Size(150, 139);
            this.groupBox_RawWeight.TabIndex = 3;
            this.groupBox_RawWeight.TabStop = false;
            this.groupBox_RawWeight.Text = "Raw Weight";
            // 
            // label_rwBR
            // 
            this.label_rwBR.AutoSize = true;
            this.label_rwBR.Location = new System.Drawing.Point(101, 76);
            this.label_rwBR.Name = "label_rwBR";
            this.label_rwBR.Size = new System.Drawing.Size(22, 13);
            this.label_rwBR.TabIndex = 0;
            this.label_rwBR.Text = "BR";
            // 
            // label_rwBL
            // 
            this.label_rwBL.AutoSize = true;
            this.label_rwBL.Location = new System.Drawing.Point(25, 76);
            this.label_rwBL.Name = "label_rwBL";
            this.label_rwBL.Size = new System.Drawing.Size(20, 13);
            this.label_rwBL.TabIndex = 0;
            this.label_rwBL.Text = "BL";
            // 
            // label_rwTR
            // 
            this.label_rwTR.AutoSize = true;
            this.label_rwTR.Location = new System.Drawing.Point(101, 32);
            this.label_rwTR.Name = "label_rwTR";
            this.label_rwTR.Size = new System.Drawing.Size(22, 13);
            this.label_rwTR.TabIndex = 0;
            this.label_rwTR.Text = "TR";
            // 
            // label_rwTL
            // 
            this.label_rwTL.AutoSize = true;
            this.label_rwTL.Location = new System.Drawing.Point(25, 32);
            this.label_rwTL.Name = "label_rwTL";
            this.label_rwTL.Size = new System.Drawing.Size(20, 13);
            this.label_rwTL.TabIndex = 0;
            this.label_rwTL.Text = "TL";
            // 
            // groupBox_OffsetWeight
            // 
            this.groupBox_OffsetWeight.Controls.Add(this.label_owWT);
            this.groupBox_OffsetWeight.Controls.Add(this.label_owTL);
            this.groupBox_OffsetWeight.Controls.Add(this.label_owTR);
            this.groupBox_OffsetWeight.Controls.Add(this.label_owBL);
            this.groupBox_OffsetWeight.Controls.Add(this.label_owBR);
            this.groupBox_OffsetWeight.Location = new System.Drawing.Point(168, 12);
            this.groupBox_OffsetWeight.Name = "groupBox_OffsetWeight";
            this.groupBox_OffsetWeight.Size = new System.Drawing.Size(150, 139);
            this.groupBox_OffsetWeight.TabIndex = 4;
            this.groupBox_OffsetWeight.TabStop = false;
            this.groupBox_OffsetWeight.Text = "Offset Weight";
            // 
            // label_owWT
            // 
            this.label_owWT.AutoSize = true;
            this.label_owWT.Location = new System.Drawing.Point(63, 113);
            this.label_owWT.Name = "label_owWT";
            this.label_owWT.Size = new System.Drawing.Size(25, 13);
            this.label_owWT.TabIndex = 1;
            this.label_owWT.Text = "WT";
            // 
            // label_owTL
            // 
            this.label_owTL.AutoSize = true;
            this.label_owTL.Location = new System.Drawing.Point(25, 32);
            this.label_owTL.Name = "label_owTL";
            this.label_owTL.Size = new System.Drawing.Size(20, 13);
            this.label_owTL.TabIndex = 0;
            this.label_owTL.Text = "TL";
            // 
            // label_owTR
            // 
            this.label_owTR.AutoSize = true;
            this.label_owTR.Location = new System.Drawing.Point(101, 32);
            this.label_owTR.Name = "label_owTR";
            this.label_owTR.Size = new System.Drawing.Size(22, 13);
            this.label_owTR.TabIndex = 0;
            this.label_owTR.Text = "TR";
            // 
            // label_owBL
            // 
            this.label_owBL.AutoSize = true;
            this.label_owBL.Location = new System.Drawing.Point(25, 76);
            this.label_owBL.Name = "label_owBL";
            this.label_owBL.Size = new System.Drawing.Size(20, 13);
            this.label_owBL.TabIndex = 0;
            this.label_owBL.Text = "BL";
            // 
            // label_owBR
            // 
            this.label_owBR.AutoSize = true;
            this.label_owBR.Location = new System.Drawing.Point(101, 76);
            this.label_owBR.Name = "label_owBR";
            this.label_owBR.Size = new System.Drawing.Size(22, 13);
            this.label_owBR.TabIndex = 0;
            this.label_owBR.Text = "BR";
            // 
            // groupBox_OffsetWeightRatio
            // 
            this.groupBox_OffsetWeightRatio.Controls.Add(this.label_owrTL);
            this.groupBox_OffsetWeightRatio.Controls.Add(this.label_owrTR);
            this.groupBox_OffsetWeightRatio.Controls.Add(this.label_owrBL);
            this.groupBox_OffsetWeightRatio.Controls.Add(this.label_owrBR);
            this.groupBox_OffsetWeightRatio.Location = new System.Drawing.Point(324, 12);
            this.groupBox_OffsetWeightRatio.Name = "groupBox_OffsetWeightRatio";
            this.groupBox_OffsetWeightRatio.Size = new System.Drawing.Size(150, 139);
            this.groupBox_OffsetWeightRatio.TabIndex = 4;
            this.groupBox_OffsetWeightRatio.TabStop = false;
            this.groupBox_OffsetWeightRatio.Text = "Offset Weight Ratio";
            // 
            // label_owrTL
            // 
            this.label_owrTL.AutoSize = true;
            this.label_owrTL.Location = new System.Drawing.Point(25, 32);
            this.label_owrTL.Name = "label_owrTL";
            this.label_owrTL.Size = new System.Drawing.Size(20, 13);
            this.label_owrTL.TabIndex = 0;
            this.label_owrTL.Text = "TL";
            // 
            // label_owrTR
            // 
            this.label_owrTR.AutoSize = true;
            this.label_owrTR.Location = new System.Drawing.Point(101, 32);
            this.label_owrTR.Name = "label_owrTR";
            this.label_owrTR.Size = new System.Drawing.Size(22, 13);
            this.label_owrTR.TabIndex = 0;
            this.label_owrTR.Text = "TR";
            // 
            // label_owrBL
            // 
            this.label_owrBL.AutoSize = true;
            this.label_owrBL.Location = new System.Drawing.Point(25, 76);
            this.label_owrBL.Name = "label_owrBL";
            this.label_owrBL.Size = new System.Drawing.Size(20, 13);
            this.label_owrBL.TabIndex = 0;
            this.label_owrBL.Text = "BL";
            // 
            // label_owrBR
            // 
            this.label_owrBR.AutoSize = true;
            this.label_owrBR.Location = new System.Drawing.Point(101, 76);
            this.label_owrBR.Name = "label_owrBR";
            this.label_owrBR.Size = new System.Drawing.Size(22, 13);
            this.label_owrBR.TabIndex = 0;
            this.label_owrBR.Text = "BR";
            // 
            // groupBox_General
            // 
            this.groupBox_General.Controls.Add(this.button_ResetDefaults);
            this.groupBox_General.Controls.Add(this.button_BluetoothAddDevice);
            this.groupBox_General.Controls.Add(this.button_Connect);
            this.groupBox_General.Location = new System.Drawing.Point(12, 157);
            this.groupBox_General.Name = "groupBox_General";
            this.groupBox_General.Size = new System.Drawing.Size(618, 78);
            this.groupBox_General.TabIndex = 0;
            this.groupBox_General.TabStop = false;
            this.groupBox_General.Text = "General";
            // 
            // button_ResetDefaults
            // 
            this.button_ResetDefaults.Location = new System.Drawing.Point(390, 19);
            this.button_ResetDefaults.Name = "button_ResetDefaults";
            this.button_ResetDefaults.Size = new System.Drawing.Size(222, 48);
            this.button_ResetDefaults.TabIndex = 3;
            this.button_ResetDefaults.Text = "Reset saved values";
            this.button_ResetDefaults.UseVisualStyleBackColor = true;
            this.button_ResetDefaults.Click += new System.EventHandler(this.button_ResetDefaults_Click);
            // 
            // button_BluetoothAddDevice
            // 
            this.button_BluetoothAddDevice.Location = new System.Drawing.Point(195, 19);
            this.button_BluetoothAddDevice.Name = "button_BluetoothAddDevice";
            this.button_BluetoothAddDevice.Size = new System.Drawing.Size(189, 48);
            this.button_BluetoothAddDevice.TabIndex = 1;
            this.button_BluetoothAddDevice.Text = "Add bluetooth Wii device";
            this.button_BluetoothAddDevice.UseVisualStyleBackColor = true;
            this.button_BluetoothAddDevice.Click += new System.EventHandler(this.button_BluetoothAddDevice_Click);
            // 
            // groupBox_BalanceRatio
            // 
            this.groupBox_BalanceRatio.Controls.Add(this.label_brDF);
            this.groupBox_BalanceRatio.Controls.Add(this.label_brX);
            this.groupBox_BalanceRatio.Controls.Add(this.label_brDR);
            this.groupBox_BalanceRatio.Controls.Add(this.label_brDL);
            this.groupBox_BalanceRatio.Controls.Add(this.label_brY);
            this.groupBox_BalanceRatio.Location = new System.Drawing.Point(480, 12);
            this.groupBox_BalanceRatio.Name = "groupBox_BalanceRatio";
            this.groupBox_BalanceRatio.Size = new System.Drawing.Size(150, 139);
            this.groupBox_BalanceRatio.TabIndex = 5;
            this.groupBox_BalanceRatio.TabStop = false;
            this.groupBox_BalanceRatio.Text = "Balance Ratio";
            // 
            // label_brDF
            // 
            this.label_brDF.AutoSize = true;
            this.label_brDF.Location = new System.Drawing.Point(65, 113);
            this.label_brDF.Name = "label_brDF";
            this.label_brDF.Size = new System.Drawing.Size(21, 13);
            this.label_brDF.TabIndex = 0;
            this.label_brDF.Text = "DF";
            // 
            // label_Status
            // 
            this.label_Status.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label_Status.Location = new System.Drawing.Point(12, 238);
            this.label_Status.Name = "label_Status";
            this.label_Status.Size = new System.Drawing.Size(618, 24);
            this.label_Status.TabIndex = 4;
            this.label_Status.Text = "STATUS";
            this.label_Status.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox_Actions
            // 
            this.groupBox_Actions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox_Actions.Controls.Add(this.tcAction);
            this.groupBox_Actions.Location = new System.Drawing.Point(636, 12);
            this.groupBox_Actions.Name = "groupBox_Actions";
            this.groupBox_Actions.Size = new System.Drawing.Size(352, 250);
            this.groupBox_Actions.TabIndex = 2;
            this.groupBox_Actions.TabStop = false;
            this.groupBox_Actions.Text = "Actions";
            // 
            // tcAction
            // 
            this.tcAction.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcAction.Controls.Add(this.tabPage1);
            this.tcAction.Controls.Add(this.tabPage2);
            this.tcAction.Location = new System.Drawing.Point(6, 19);
            this.tcAction.Name = "tcAction";
            this.tcAction.SelectedIndex = 0;
            this.tcAction.Size = new System.Drawing.Size(340, 225);
            this.tcAction.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(332, 199);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(332, 199);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // checkBox_DisableActions
            // 
            this.checkBox_DisableActions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkBox_DisableActions.AutoSize = true;
            this.checkBox_DisableActions.Location = new System.Drawing.Point(654, 268);
            this.checkBox_DisableActions.Name = "checkBox_DisableActions";
            this.checkBox_DisableActions.Size = new System.Drawing.Size(113, 17);
            this.checkBox_DisableActions.TabIndex = 0;
            this.checkBox_DisableActions.Text = "Disable All Actions";
            this.checkBox_DisableActions.UseVisualStyleBackColor = true;
            this.checkBox_DisableActions.CheckedChanged += new System.EventHandler(this.checkBox_DisableActions_CheckedChanged);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 297);
            this.Controls.Add(this.groupBox_Actions);
            this.Controls.Add(this.checkBox_DisableActions);
            this.Controls.Add(this.label_Status);
            this.Controls.Add(this.groupBox_BalanceRatio);
            this.Controls.Add(this.groupBox_General);
            this.Controls.Add(this.groupBox_OffsetWeightRatio);
            this.Controls.Add(this.groupBox_OffsetWeight);
            this.Controls.Add(this.groupBox_RawWeight);
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Wii Balance Walker - Version 0.4";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.groupBox_RawWeight.ResumeLayout(false);
            this.groupBox_RawWeight.PerformLayout();
            this.groupBox_OffsetWeight.ResumeLayout(false);
            this.groupBox_OffsetWeight.PerformLayout();
            this.groupBox_OffsetWeightRatio.ResumeLayout(false);
            this.groupBox_OffsetWeightRatio.PerformLayout();
            this.groupBox_General.ResumeLayout(false);
            this.groupBox_BalanceRatio.ResumeLayout(false);
            this.groupBox_BalanceRatio.PerformLayout();
            this.groupBox_Actions.ResumeLayout(false);
            this.tcAction.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_rwWT;
        private System.Windows.Forms.Button button_Connect;
        private System.Windows.Forms.Label label_brX;
        private System.Windows.Forms.Label label_brY;
        private System.Windows.Forms.Label label_brDL;
        private System.Windows.Forms.Label label_brDR;
        private System.Windows.Forms.GroupBox groupBox_RawWeight;
        private System.Windows.Forms.Label label_rwBR;
        private System.Windows.Forms.Label label_rwBL;
        private System.Windows.Forms.Label label_rwTR;
        private System.Windows.Forms.Label label_rwTL;
        private System.Windows.Forms.GroupBox groupBox_OffsetWeight;
        private System.Windows.Forms.GroupBox groupBox_OffsetWeightRatio;
        private System.Windows.Forms.Label label_owTL;
        private System.Windows.Forms.Label label_owTR;
        private System.Windows.Forms.Label label_owBL;
        private System.Windows.Forms.Label label_owBR;
        private System.Windows.Forms.Label label_owrTL;
        private System.Windows.Forms.Label label_owrTR;
        private System.Windows.Forms.Label label_owrBL;
        private System.Windows.Forms.Label label_owrBR;
        private System.Windows.Forms.GroupBox groupBox_General;
        private System.Windows.Forms.GroupBox groupBox_BalanceRatio;
        private System.Windows.Forms.Label label_brDF;
        private System.Windows.Forms.Label label_Status;
        private System.Windows.Forms.Button button_BluetoothAddDevice;
        private System.Windows.Forms.GroupBox groupBox_Actions;
        private System.Windows.Forms.Button button_ResetDefaults;
        private System.Windows.Forms.CheckBox checkBox_DisableActions;
        private System.Windows.Forms.Label label_owWT;
        private System.Windows.Forms.TabControl tcAction;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
    }
}

