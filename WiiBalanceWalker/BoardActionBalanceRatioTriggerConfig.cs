﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WiiBalanceWalker
{
    public partial class BoardActionBalanceRatioTriggerConfig : UserControl
    {
        public BoardActionBalanceRatioTriggerConfig()
        {
            InitializeComponent();
        }

        private void BoardActionBalanceRatioTriggerConfig_Load(object sender, EventArgs e)
        {
            // Load trigger settings.
            numericUpDown_TLR.Value = Properties.Settings.Default.TriggerLeftRight;
            numericUpDown_TFB.Value = Properties.Settings.Default.TriggerForwardBackward;
            numericUpDown_TMLR.Value = Properties.Settings.Default.TriggerModifierLeftRight;
            numericUpDown_TMFB.Value = Properties.Settings.Default.TriggerModifierForwardBackward;
        }

        private void numericUpDown_TLR_ValueChanged(object sender, EventArgs e)
        {
            int Out = (int)numericUpDown_TLR.Value;
            //BalanceBoard.LeftRightBalanceRatioTrigger = Out;
            Properties.Settings.Default.TriggerLeftRight = Out;
            Properties.Settings.Default.Save();
        }

        private void numericUpDown_TFB_ValueChanged(object sender, EventArgs e)
        {
            int Out = (int)numericUpDown_TFB.Value;
            //BalanceBoard.TopDownBalanceRatioTrigger = Out;
            Properties.Settings.Default.TriggerForwardBackward = Out;
            Properties.Settings.Default.Save();
        }

        private void numericUpDown_TMLR_ValueChanged(object sender, EventArgs e)
        {
            int Out = (int)numericUpDown_TMLR.Value;
            //BalanceBoard.ModifierLeftRightBalanceRatioTrigger = Out;
            Properties.Settings.Default.TriggerModifierLeftRight = Out;
            Properties.Settings.Default.Save();
        }

        private void numericUpDown_TMFB_ValueChanged(object sender, EventArgs e)
        {
            int Out = (int)numericUpDown_TMFB.Value;
            //BalanceBoard.ModifierTopDownBalanceRatioTrigger = Out;
            Properties.Settings.Default.TriggerModifierForwardBackward = Out;
            Properties.Settings.Default.Save();
        }

        private void button_SetCenterOffset_Click(object sender, EventArgs e)
        {
            ((Button)sender).Enabled = false;
        }
    }
}
