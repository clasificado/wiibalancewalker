﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WiiBalanceWalker
{
    interface IBoardAction
    {
        Control ConfigControl { get; set; }
        String ConfigControlTitle { get; }
        String ConfigControlTypeName { get; }
        BalanceBoardManager.TickData ActionTick(BalanceBoardManager.TickData send);
        void Init();
        void Stop();
    }
}