﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WiiBalanceWalker
{
    class BoardActionBalanceRatioTrigger : IBoardAction
    {
        DateTime jumpTime = DateTime.UtcNow;
        float naCorners = 0f;
        float oaTopLeft = 0f;
        float oaTopRight = 0f;
        float oaBottomLeft = 0f;
        float oaBottomRight = 0f;
        bool setCenterOffset = false;

        public string ConfigControlTitle
        {
            get { return "Balance Ratio Trigger"; }
        }

        public string ConfigControlTypeName
        {
            get { return "WiiBalanceWalker.BoardActionBalanceRatioTriggerConfig"; }
        }

        public System.Windows.Forms.Control ConfigControl
        {
            get;
            set;
        }
        Label lblCurrentAction;
        Button btnSetCenterOffset;
        NumericUpDown numericUpDown_TMFB;
        NumericUpDown numericUpDown_TMLR;
        NumericUpDown numericUpDown_TFB;
        NumericUpDown numericUpDown_TLR;
        public void Init()
        {
            btnSetCenterOffset = (Button)ConfigControl.Controls.Find("button_SetCenterOffset", true)[0];
            lblCurrentAction = (Label)this.ConfigControl.Controls.Find("lblCurrentAction", true)[0];
            numericUpDown_TMFB = (NumericUpDown)this.ConfigControl.Controls.Find("numericUpDown_TMFB", true)[0];
            numericUpDown_TMLR = (NumericUpDown)this.ConfigControl.Controls.Find("numericUpDown_TMLR", true)[0];
            numericUpDown_TFB = (NumericUpDown)this.ConfigControl.Controls.Find("numericUpDown_TFB", true)[0];
            numericUpDown_TLR = (NumericUpDown)this.ConfigControl.Controls.Find("numericUpDown_TLR", true)[0];
        }

        ActionList actionList = new ActionList();
        public BalanceBoardManager.TickData ActionTick(BalanceBoardManager.TickData send)
        {
            var statusData = send.StatusData;
            var wiimoteState = send.WiimoteState;

            // Get the current raw sensor KG values.

            var rwWeight = wiimoteState.BalanceBoardState.WeightKg;
            var rwTopLeft = wiimoteState.BalanceBoardState.SensorValuesKg.TopLeft;
            var rwTopRight = wiimoteState.BalanceBoardState.SensorValuesKg.TopRight;
            var rwBottomLeft = wiimoteState.BalanceBoardState.SensorValuesKg.BottomLeft;
            var rwBottomRight = wiimoteState.BalanceBoardState.SensorValuesKg.BottomRight;

            // The alternative .SensorValuesRaw is not adjusted with 17KG and 34KG calibration data, but does that make for better or worse control?
            //
            //var rwTopLeft     = wiiDevice.WiimoteState.BalanceBoardState.SensorValuesRaw.TopLeft     - wiiDevice.WiimoteState.BalanceBoardState.CalibrationInfo.Kg0.TopLeft;
            //var rwTopRight    = wiiDevice.WiimoteState.BalanceBoardState.SensorValuesRaw.TopRight    - wiiDevice.WiimoteState.BalanceBoardState.CalibrationInfo.Kg0.TopRight;
            //var rwBottomLeft  = wiiDevice.WiimoteState.BalanceBoardState.SensorValuesRaw.BottomLeft  - wiiDevice.WiimoteState.BalanceBoardState.CalibrationInfo.Kg0.BottomLeft;
            //var rwBottomRight = wiiDevice.WiimoteState.BalanceBoardState.SensorValuesRaw.BottomRight - wiiDevice.WiimoteState.BalanceBoardState.CalibrationInfo.Kg0.BottomRight;

            // Show the raw sensor values.

            //label_rwWT.Text
            statusData.RawWeightTotal = rwWeight;
            statusData.RawWeightTopLeft = rwTopLeft;
            statusData.RawWeightTopRight = rwTopRight;
            statusData.RawWeightBottomLeft = rwBottomLeft;
            statusData.RawWeightBottomRight = rwBottomRight;

            // Prevent negative values by tracking lowest possible value and making it a zero based offset.

            if (rwTopLeft < naCorners) naCorners = rwTopLeft;
            if (rwTopRight < naCorners) naCorners = rwTopRight;
            if (rwBottomLeft < naCorners) naCorners = rwBottomLeft;
            if (rwBottomRight < naCorners) naCorners = rwBottomRight;

            // Negative total weight is reset to zero as jumping or lifting the board causes negative spikes, which would break 'in use' checks.

            var owWeight = rwWeight < 0f ? 0f : rwWeight;
            var owTopLeft = rwTopLeft -= naCorners;
            var owTopRight = rwTopRight -= naCorners;
            var owBottomLeft = rwBottomLeft -= naCorners;
            var owBottomRight = rwBottomRight -= naCorners;

            // Get offset that would make current values the center of mass.
            setCenterOffset = !(btnSetCenterOffset).Enabled;
            if (setCenterOffset)
            {
                this.setCenterOffset = false;
                btnSetCenterOffset.Enabled = true;

                var rwHighest = Math.Max(Math.Max(rwTopLeft, rwTopRight), Math.Max(rwBottomLeft, rwBottomRight));

                oaTopLeft = rwHighest - rwTopLeft;
                oaTopRight = rwHighest - rwTopRight;
                oaBottomLeft = rwHighest - rwBottomLeft;
                oaBottomRight = rwHighest - rwBottomRight;
            }

            // Keep values only when board is being used, otherwise offsets and small value jitters can trigger unwanted actions.

            if (owWeight > 0f)
            {
                owTopLeft += oaTopLeft;
                owTopRight += oaTopRight;
                owBottomLeft += oaBottomLeft;
                owBottomRight += oaBottomRight;
            }
            else
            {
                owTopLeft = 0;
                owTopRight = 0;
                owBottomLeft = 0;
                owBottomRight = 0;
            }

            //label_owWT.Text
            //statusData.OffsetWeightTotal = owWeight.ToString("0.0");
            statusData.OffsetWeightTotal = owWeight;
            statusData.OffsetWeightTopLeft = owTopLeft.ToString("0.0") + "\r\n" + oaTopLeft.ToString("0.0");
            statusData.OffsetWeightTopRight = owTopRight.ToString("0.0") + "\r\n" + oaTopRight.ToString("0.0");
            statusData.OffsetWeightBottomLeft = owBottomLeft.ToString("0.0") + "\r\n" + oaBottomLeft.ToString("0.0");
            statusData.OffsetWeightBottomRight = owBottomRight.ToString("0.0") + "\r\n" + oaBottomRight.ToString("0.0");

            // Calculate each weight ratio.
            var owrPercentage = 100 / (owTopLeft + owTopRight + owBottomLeft + owBottomRight);
            var owrTopLeft = owrPercentage * owTopLeft;
            var owrTopRight = owrPercentage * owTopRight;
            var owrBottomLeft = owrPercentage * owBottomLeft;
            var owrBottomRight = owrPercentage * owBottomRight;

            //label_owrTL.Text
            statusData.OffsetWeightRatioTopLeft = owrTopLeft.ToString("0.0");
            statusData.OffsetWeightRatioTopRight = owrTopRight.ToString("0.0");
            statusData.OffsetWeightRatioBottomLeft = owrBottomLeft.ToString("0.0");
            statusData.OffsetWeightRatioBottomRight = owrBottomRight.ToString("0.0");

            // Calculate balance ratio.

            var brX = owrBottomRight + owrTopRight;
            var brY = owrBottomRight + owrBottomLeft;

            //label_brX.Text
            statusData.BalanceRatioX = brX;
            statusData.BalanceRatioY = brY;

            // Diagonal ratio used for turning on the spot.

            var brDL = owrPercentage * (owBottomLeft + owTopRight);
            var brDR = owrPercentage * (owBottomRight + owTopLeft);
            var brDF = Math.Abs(brDL - brDR);

            //label_brDL.Text
            statusData.BalanceRatioDiagonalLeft = brDL.ToString("0.0");
            statusData.BalanceRatioDiagonalRight = brDR.ToString("0.0");
            statusData.BalanceRatioDF = brDF.ToString("0.0");



            // Display actions.
            /*
            Out.status = "Result: ";

            if (sendForward) Out.status += "Forward";
            if (sendLeft) Out.status += "Left";
            if (sendBackward) Out.status += "Backward";
            if (sendRight) Out.status += "Right";
            if (sendModifier) Out.status += " + Modifier";
            if (sendJump) Out.status += "Jump";
            if (sendDiagonalLeft) Out.status += "Diagonal Left";
            if (sendDiagonalRight) Out.status += "Diagonal Right";
            **/

            //if (this.DisableActions) statusData.status += " ( DISABLED )";

            var LeftRightBalanceRatioTrigger = numericUpDown_TLR.Value;
            var TopDownBalanceRatioTrigger = numericUpDown_TFB.Value;
            var ModifierLeftRightBalanceRatioTrigger = numericUpDown_TMLR.Value;
            var ModifierTopDownBalanceRatioTrigger = numericUpDown_TMFB.Value;

            // Convert sensor values into actions.
            var sendLeft = false;
            var sendRight = false;
            var sendForward = false;
            var sendBackward = false;
            var sendModifier = false;
            var sendJump = false;
            var sendDiagonalLeft = false;
            var sendDiagonalRight = false;

            //if (brX < (float)(50 - numericUpDown_TLR.Value)) sendLeft = true;
            if (brX < (float)(50 - LeftRightBalanceRatioTrigger)) sendLeft = true;
            if (brX > (float)(50 + LeftRightBalanceRatioTrigger)) sendRight = true;
            if (brY < (float)(50 - TopDownBalanceRatioTrigger)) sendForward = true;
            if (brY > (float)(50 + TopDownBalanceRatioTrigger)) sendBackward = true;

            if (brX < (float)(50 - ModifierLeftRightBalanceRatioTrigger)) sendModifier = true;
            else if (brX > (float)(50 + ModifierLeftRightBalanceRatioTrigger)) sendModifier = true;
            else if (brY < (float)(50 - ModifierTopDownBalanceRatioTrigger)) sendModifier = true;
            else if (brY > (float)(50 + ModifierTopDownBalanceRatioTrigger)) sendModifier = true;

            // Detect jump but use a time limit to stop it being active while off the board.

            if (owWeight < 1f)
            {
                if (DateTime.UtcNow.Subtract(jumpTime).Seconds < 2) sendJump = true;
            }
            else
            {
                jumpTime = DateTime.UtcNow;
            }

            // Check for diagonal pressure only when no other movement actions are active.
            if (!sendLeft && !sendRight && !sendForward && !sendBackward && brDF > 15)
            {
                if (brDL > brDR) sendDiagonalLeft = true;
                else sendDiagonalRight = true;
            }
            var Status = "";
            if (sendLeft)
            {
                Status = " Left";
                send.XInputData.GamepadData = send.XInputData.GamepadData | XInputData.GamepadEnum.XINPUT_GAMEPAD_DPAD_LEFT;

            }
            if (sendRight)
            {
                Status = " Right";
                send.XInputData.GamepadData = send.XInputData.GamepadData | XInputData.GamepadEnum.XINPUT_GAMEPAD_DPAD_RIGHT;
            }
            if (sendForward)
            {
                Status = " Forward";
                send.XInputData.GamepadData = send.XInputData.GamepadData | XInputData.GamepadEnum.XINPUT_GAMEPAD_DPAD_UP;
            }
            if (sendBackward)
            {
                Status = " Backward";
                send.XInputData.GamepadData = send.XInputData.GamepadData | XInputData.GamepadEnum.XINPUT_GAMEPAD_DPAD_DOWN;
            }
            if (sendJump)
            {
                Status = " Jump";
                send.XInputData.GamepadData = send.XInputData.GamepadData | XInputData.GamepadEnum.XINPUT_GAMEPAD_A;
            }
            if (sendModifier)
            {
                Status = " Modifier";
                send.XInputData.GamepadData = send.XInputData.GamepadData | XInputData.GamepadEnum.XINPUT_GAMEPAD_B;
            }
            if (sendDiagonalLeft)
            {
                Status = " Diagonal Left";
                send.XInputData.GamepadData = send.XInputData.GamepadData | XInputData.GamepadEnum.XINPUT_GAMEPAD_X;
            }
            if (sendDiagonalRight)
            {
                Status = " Diagonal Right";
                send.XInputData.GamepadData = send.XInputData.GamepadData | XInputData.GamepadEnum.XINPUT_GAMEPAD_Y;
            }
            lblCurrentAction.Text = Status;

            return send;
        }

        public void Stop()
        {
        }
    }
}