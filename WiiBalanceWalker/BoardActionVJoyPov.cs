﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using vJoyInterfaceWrap;

namespace WiiBalanceWalker
{
    class BoardActionVJoyPov : IBoardAction
    {
        public string ConfigControlTitle
        {
            get { return "VJoyPov"; }
        }

        public string ConfigControlTypeName
        {
            get { return "WiiBalanceWalker.BoardActionVJoyPovConfig"; }
        }
        public vJoy joystick;
        public vJoy.JoystickState iReport;
        public uint id = 1;

        int nButtons;
        int ContPovNumber;
        int DiscPovNumber;

        long maxval = 0;

        public System.Windows.Forms.Control ConfigControl
        {
            get;
            set;
        }
        public void Init()
        {
            joystick = new vJoy();
            iReport = new vJoy.JoystickState();


            // Device ID can only be in the range 1-16
            if (id <= 0 || id > 16)
            {
                WriteLine("Illegal device ID {0}\nExit!", id);
                return;
            }

            // Get the driver attributes (Vendor ID, Product ID, Version Number)
            if (!joystick.vJoyEnabled())
            {
                WriteLine("vJoy driver not enabled: Failed Getting vJoy attributes.\n");
            }
            //else
                //WriteLine("Vendor: {0}\nProduct :{1}\nVersion Number:{2}\n", joystick.GetvJoyManufacturerString(), joystick.GetvJoyProductString(), joystick.GetvJoySerialNumberString());

            // Get the state of the requested device
            VjdStat status = joystick.GetVJDStatus(id);
            switch (status)
            {
                case VjdStat.VJD_STAT_OWN:
                    WriteLine("vJoy Device {0} is already owned by this feeder\n", id);
                    break;
                case VjdStat.VJD_STAT_FREE:
                    //WriteLine("vJoy Device {0} is free\n", id);
                    break;
                case VjdStat.VJD_STAT_BUSY:
                    WriteLine("vJoy Device {0} is already owned by another feeder\nCannot continue\n", id);
                    return;
                case VjdStat.VJD_STAT_MISS:
                    WriteLine("vJoy Device {0} is not installed or disabled\nCannot continue\n", id);
                    return;
                default:
                    WriteLine("vJoy Device {0} general error\nCannot continue\n", id);
                    return;
            };

            // Check which axes are supported
            bool AxisX = joystick.GetVJDAxisExist(id, HID_USAGES.HID_USAGE_X);
            bool AxisY = joystick.GetVJDAxisExist(id, HID_USAGES.HID_USAGE_Y);
            bool AxisZ = joystick.GetVJDAxisExist(id, HID_USAGES.HID_USAGE_Z);
            bool AxisRX = joystick.GetVJDAxisExist(id, HID_USAGES.HID_USAGE_RX);
            bool AxisRZ = joystick.GetVJDAxisExist(id, HID_USAGES.HID_USAGE_RZ);
            // Get the number of buttons and POV Hat switchessupported by this vJoy device
            nButtons = joystick.GetVJDButtonNumber(id);
            ContPovNumber = joystick.GetVJDContPovNumber(id);
            DiscPovNumber = joystick.GetVJDDiscPovNumber(id);
            joystick.SetDiscPov(-1, id, 4); //nothing -1

            // Print results
            /*WriteLine("\nvJoy Device {0} capabilities:\n", id);
            WriteLine("Numner of buttons\t\t{0}\n", nButtons);
            WriteLine("Numner of Continuous POVs\t{0}\n", ContPovNumber);
            WriteLine("Numner of Descrete POVs\t\t{0}\n", DiscPovNumber);
            WriteLine("Axis X\t\t{0}\n", AxisX ? "Yes" : "No");
            WriteLine("Axis Y\t\t{0}\n", AxisX ? "Yes" : "No");
            WriteLine("Axis Z\t\t{0}\n", AxisX ? "Yes" : "No");
            WriteLine("Axis Rx\t\t{0}\n", AxisRX ? "Yes" : "No");
            WriteLine("Axis Rz\t\t{0}\n", AxisRZ ? "Yes" : "No");
             * */

            // Test if DLL matches the driver
            UInt32 DllVer = 0, DrvVer = 0;
            bool match = joystick.DriverMatch(ref DllVer, ref DrvVer);
            if (!match)
            {
                WriteLine("Version of Driver ({0:X}) does NOT match DLL Version ({1:X})\n", DrvVer, DllVer);
            }

            // Acquire the target
            if ((status == VjdStat.VJD_STAT_OWN) || ((status == VjdStat.VJD_STAT_FREE) && (!joystick.AcquireVJD(id))))
            {
                WriteLine("Failed to acquire vJoy device number {0}.\n", id);
            }

            joystick.GetVJDAxisMax(id, HID_USAGES.HID_USAGE_X, ref maxval);

            // Reset this device to default values
            joystick.ResetVJD(id);
        }

        private void WriteLine(params object[] o)
        {
            var Status = "VJOYPOV: ";
            Status += String.Join(", ", o);
            throw new ApplicationException(Status);
        }

        public BalanceBoardManager.TickData ActionTick(BalanceBoardManager.TickData send)
        {
            var wiimoteState = send.WiimoteState;
            bool res;
            
            // If Continuous POV hat switches installed - make them go round
            // For high values - put the switches in neutral state
           /* if (ContPovNumber > 0)
            {
                if ((count * 70) < 30000)
                {
                    res = joystick.SetContPov(((int)count * 70), id, 1);
                    res = joystick.SetContPov(((int)count * 70) + 2000, id, 2);
                    res = joystick.SetContPov(((int)count * 70) + 4000, id, 3);
                    res = joystick.SetContPov(((int)count * 70) + 6000, id, 4);
                }
                else
                {
                    res = joystick.SetContPov(-1, id, 1);
                    res = joystick.SetContPov(-1, id, 2);
                    res = joystick.SetContPov(-1, id, 3);
                    res = joystick.SetContPov(-1, id, 4);
                };
            };*/

            // If Discrete POV hat switches installed - make them go round
            // From time to time - put the switches in neutral state
            if (DiscPovNumber > 0)
            {
                var set = false;
                if ((send.XInputData.GamepadData & XInputData.GamepadEnum.XINPUT_GAMEPAD_DPAD_UP) != 0)
                {
                    set = true;
                    joystick.SetDiscPov(0, id, 1); //top 0
                }
                if ((send.XInputData.GamepadData & XInputData.GamepadEnum.XINPUT_GAMEPAD_DPAD_RIGHT) != 0) 
                {
                    set = true;
                    joystick.SetDiscPov(1, id, 1); //right 1
                }
                if ((send.XInputData.GamepadData & XInputData.GamepadEnum.XINPUT_GAMEPAD_DPAD_DOWN) != 0)
                {
                    set = true;
                    joystick.SetDiscPov(2, id, 1); //bottom 2
                }
                if ((send.XInputData.GamepadData & XInputData.GamepadEnum.XINPUT_GAMEPAD_DPAD_LEFT) != 0)
                {
                    set = true;
                    joystick.SetDiscPov(3, id, 1); //left 3
                }
                if (!set)
                {
                    joystick.SetDiscPov(-1, id, 1); //nothing -1
                }
            };
            return send;
        }

        public void Stop()
        {
        }
    }
}