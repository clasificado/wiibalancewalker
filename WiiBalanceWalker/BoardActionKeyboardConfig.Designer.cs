﻿namespace WiiBalanceWalker
{
    partial class BoardActionKeyboardConfig
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.numericUpDown_ADR = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_ADL = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_AJ = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_AM = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_AB = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_AF = new System.Windows.Forms.NumericUpDown();
            this.comboBox_ADR = new System.Windows.Forms.ComboBox();
            this.comboBox_AJ = new System.Windows.Forms.ComboBox();
            this.comboBox_ADL = new System.Windows.Forms.ComboBox();
            this.label_ActionJump = new System.Windows.Forms.Label();
            this.label_ActionDiagonalRight = new System.Windows.Forms.Label();
            this.label_ActionDiagonalLeft = new System.Windows.Forms.Label();
            this.label_ActionModifier = new System.Windows.Forms.Label();
            this.label_ActionBackward = new System.Windows.Forms.Label();
            this.label_ActionForward = new System.Windows.Forms.Label();
            this.label_ActionRight = new System.Windows.Forms.Label();
            this.label_ActionLeft = new System.Windows.Forms.Label();
            this.numericUpDown_AR = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_AL = new System.Windows.Forms.NumericUpDown();
            this.comboBox_AM = new System.Windows.Forms.ComboBox();
            this.comboBox_AF = new System.Windows.Forms.ComboBox();
            this.comboBox_AB = new System.Windows.Forms.ComboBox();
            this.comboBox_AR = new System.Windows.Forms.ComboBox();
            this.comboBox_AL = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ADR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ADL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_AJ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_AM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_AB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_AF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_AR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_AL)).BeginInit();
            this.SuspendLayout();
            // 
            // numericUpDown_ADR
            // 
            this.numericUpDown_ADR.Location = new System.Drawing.Point(227, 193);
            this.numericUpDown_ADR.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDown_ADR.Minimum = new decimal(new int[] {
            999,
            0,
            0,
            -2147483648});
            this.numericUpDown_ADR.Name = "numericUpDown_ADR";
            this.numericUpDown_ADR.Size = new System.Drawing.Size(49, 20);
            this.numericUpDown_ADR.TabIndex = 41;
            // 
            // numericUpDown_ADL
            // 
            this.numericUpDown_ADL.Location = new System.Drawing.Point(227, 166);
            this.numericUpDown_ADL.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDown_ADL.Minimum = new decimal(new int[] {
            999,
            0,
            0,
            -2147483648});
            this.numericUpDown_ADL.Name = "numericUpDown_ADL";
            this.numericUpDown_ADL.Size = new System.Drawing.Size(49, 20);
            this.numericUpDown_ADL.TabIndex = 39;
            // 
            // numericUpDown_AJ
            // 
            this.numericUpDown_AJ.Location = new System.Drawing.Point(227, 139);
            this.numericUpDown_AJ.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDown_AJ.Minimum = new decimal(new int[] {
            999,
            0,
            0,
            -2147483648});
            this.numericUpDown_AJ.Name = "numericUpDown_AJ";
            this.numericUpDown_AJ.Size = new System.Drawing.Size(49, 20);
            this.numericUpDown_AJ.TabIndex = 37;
            // 
            // numericUpDown_AM
            // 
            this.numericUpDown_AM.Location = new System.Drawing.Point(227, 112);
            this.numericUpDown_AM.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDown_AM.Minimum = new decimal(new int[] {
            999,
            0,
            0,
            -2147483648});
            this.numericUpDown_AM.Name = "numericUpDown_AM";
            this.numericUpDown_AM.Size = new System.Drawing.Size(49, 20);
            this.numericUpDown_AM.TabIndex = 35;
            // 
            // numericUpDown_AB
            // 
            this.numericUpDown_AB.Location = new System.Drawing.Point(227, 85);
            this.numericUpDown_AB.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDown_AB.Minimum = new decimal(new int[] {
            999,
            0,
            0,
            -2147483648});
            this.numericUpDown_AB.Name = "numericUpDown_AB";
            this.numericUpDown_AB.Size = new System.Drawing.Size(49, 20);
            this.numericUpDown_AB.TabIndex = 33;
            // 
            // numericUpDown_AF
            // 
            this.numericUpDown_AF.Location = new System.Drawing.Point(227, 58);
            this.numericUpDown_AF.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDown_AF.Minimum = new decimal(new int[] {
            999,
            0,
            0,
            -2147483648});
            this.numericUpDown_AF.Name = "numericUpDown_AF";
            this.numericUpDown_AF.Size = new System.Drawing.Size(49, 20);
            this.numericUpDown_AF.TabIndex = 31;
            // 
            // comboBox_ADR
            // 
            this.comboBox_ADR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_ADR.FormattingEnabled = true;
            this.comboBox_ADR.Location = new System.Drawing.Point(96, 193);
            this.comboBox_ADR.Name = "comboBox_ADR";
            this.comboBox_ADR.Size = new System.Drawing.Size(125, 21);
            this.comboBox_ADR.TabIndex = 40;
            // 
            // comboBox_AJ
            // 
            this.comboBox_AJ.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_AJ.FormattingEnabled = true;
            this.comboBox_AJ.Location = new System.Drawing.Point(96, 139);
            this.comboBox_AJ.Name = "comboBox_AJ";
            this.comboBox_AJ.Size = new System.Drawing.Size(125, 21);
            this.comboBox_AJ.TabIndex = 36;
            // 
            // comboBox_ADL
            // 
            this.comboBox_ADL.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_ADL.FormattingEnabled = true;
            this.comboBox_ADL.Location = new System.Drawing.Point(96, 166);
            this.comboBox_ADL.Name = "comboBox_ADL";
            this.comboBox_ADL.Size = new System.Drawing.Size(125, 21);
            this.comboBox_ADL.TabIndex = 38;
            // 
            // label_ActionJump
            // 
            this.label_ActionJump.AutoSize = true;
            this.label_ActionJump.Location = new System.Drawing.Point(7, 142);
            this.label_ActionJump.Name = "label_ActionJump";
            this.label_ActionJump.Size = new System.Drawing.Size(38, 13);
            this.label_ActionJump.TabIndex = 25;
            this.label_ActionJump.Text = "- Jump";
            // 
            // label_ActionDiagonalRight
            // 
            this.label_ActionDiagonalRight.AutoSize = true;
            this.label_ActionDiagonalRight.Location = new System.Drawing.Point(7, 196);
            this.label_ActionDiagonalRight.Name = "label_ActionDiagonalRight";
            this.label_ActionDiagonalRight.Size = new System.Drawing.Size(83, 13);
            this.label_ActionDiagonalRight.TabIndex = 24;
            this.label_ActionDiagonalRight.Text = "- Diagonal Right";
            // 
            // label_ActionDiagonalLeft
            // 
            this.label_ActionDiagonalLeft.AutoSize = true;
            this.label_ActionDiagonalLeft.Location = new System.Drawing.Point(7, 169);
            this.label_ActionDiagonalLeft.Name = "label_ActionDiagonalLeft";
            this.label_ActionDiagonalLeft.Size = new System.Drawing.Size(76, 13);
            this.label_ActionDiagonalLeft.TabIndex = 18;
            this.label_ActionDiagonalLeft.Text = "- Diagonal Left";
            // 
            // label_ActionModifier
            // 
            this.label_ActionModifier.AutoSize = true;
            this.label_ActionModifier.Location = new System.Drawing.Point(7, 115);
            this.label_ActionModifier.Name = "label_ActionModifier";
            this.label_ActionModifier.Size = new System.Drawing.Size(50, 13);
            this.label_ActionModifier.TabIndex = 23;
            this.label_ActionModifier.Text = "- Modifier";
            // 
            // label_ActionBackward
            // 
            this.label_ActionBackward.AutoSize = true;
            this.label_ActionBackward.Location = new System.Drawing.Point(7, 88);
            this.label_ActionBackward.Name = "label_ActionBackward";
            this.label_ActionBackward.Size = new System.Drawing.Size(61, 13);
            this.label_ActionBackward.TabIndex = 22;
            this.label_ActionBackward.Text = "- Backward";
            // 
            // label_ActionForward
            // 
            this.label_ActionForward.AutoSize = true;
            this.label_ActionForward.Location = new System.Drawing.Point(7, 61);
            this.label_ActionForward.Name = "label_ActionForward";
            this.label_ActionForward.Size = new System.Drawing.Size(51, 13);
            this.label_ActionForward.TabIndex = 21;
            this.label_ActionForward.Text = "- Forward";
            // 
            // label_ActionRight
            // 
            this.label_ActionRight.AutoSize = true;
            this.label_ActionRight.Location = new System.Drawing.Point(7, 34);
            this.label_ActionRight.Name = "label_ActionRight";
            this.label_ActionRight.Size = new System.Drawing.Size(38, 13);
            this.label_ActionRight.TabIndex = 20;
            this.label_ActionRight.Text = "- Right";
            // 
            // label_ActionLeft
            // 
            this.label_ActionLeft.AutoSize = true;
            this.label_ActionLeft.Location = new System.Drawing.Point(7, 7);
            this.label_ActionLeft.Name = "label_ActionLeft";
            this.label_ActionLeft.Size = new System.Drawing.Size(31, 13);
            this.label_ActionLeft.TabIndex = 19;
            this.label_ActionLeft.Text = "- Left";
            // 
            // numericUpDown_AR
            // 
            this.numericUpDown_AR.Location = new System.Drawing.Point(227, 31);
            this.numericUpDown_AR.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDown_AR.Minimum = new decimal(new int[] {
            999,
            0,
            0,
            -2147483648});
            this.numericUpDown_AR.Name = "numericUpDown_AR";
            this.numericUpDown_AR.Size = new System.Drawing.Size(49, 20);
            this.numericUpDown_AR.TabIndex = 29;
            // 
            // numericUpDown_AL
            // 
            this.numericUpDown_AL.Location = new System.Drawing.Point(227, 4);
            this.numericUpDown_AL.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDown_AL.Minimum = new decimal(new int[] {
            999,
            0,
            0,
            -2147483648});
            this.numericUpDown_AL.Name = "numericUpDown_AL";
            this.numericUpDown_AL.Size = new System.Drawing.Size(49, 20);
            this.numericUpDown_AL.TabIndex = 27;
            // 
            // comboBox_AM
            // 
            this.comboBox_AM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_AM.FormattingEnabled = true;
            this.comboBox_AM.Location = new System.Drawing.Point(96, 112);
            this.comboBox_AM.Name = "comboBox_AM";
            this.comboBox_AM.Size = new System.Drawing.Size(125, 21);
            this.comboBox_AM.TabIndex = 34;
            // 
            // comboBox_AF
            // 
            this.comboBox_AF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_AF.FormattingEnabled = true;
            this.comboBox_AF.Location = new System.Drawing.Point(96, 58);
            this.comboBox_AF.Name = "comboBox_AF";
            this.comboBox_AF.Size = new System.Drawing.Size(125, 21);
            this.comboBox_AF.TabIndex = 30;
            // 
            // comboBox_AB
            // 
            this.comboBox_AB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_AB.FormattingEnabled = true;
            this.comboBox_AB.Location = new System.Drawing.Point(96, 85);
            this.comboBox_AB.Name = "comboBox_AB";
            this.comboBox_AB.Size = new System.Drawing.Size(125, 21);
            this.comboBox_AB.TabIndex = 32;
            // 
            // comboBox_AR
            // 
            this.comboBox_AR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_AR.FormattingEnabled = true;
            this.comboBox_AR.Location = new System.Drawing.Point(96, 31);
            this.comboBox_AR.Name = "comboBox_AR";
            this.comboBox_AR.Size = new System.Drawing.Size(125, 21);
            this.comboBox_AR.TabIndex = 28;
            // 
            // comboBox_AL
            // 
            this.comboBox_AL.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_AL.FormattingEnabled = true;
            this.comboBox_AL.Location = new System.Drawing.Point(96, 4);
            this.comboBox_AL.Name = "comboBox_AL";
            this.comboBox_AL.Size = new System.Drawing.Size(125, 21);
            this.comboBox_AL.TabIndex = 26;
            // 
            // BoardActionKeyboardConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.numericUpDown_ADR);
            this.Controls.Add(this.numericUpDown_ADL);
            this.Controls.Add(this.numericUpDown_AJ);
            this.Controls.Add(this.numericUpDown_AM);
            this.Controls.Add(this.numericUpDown_AB);
            this.Controls.Add(this.numericUpDown_AF);
            this.Controls.Add(this.comboBox_ADR);
            this.Controls.Add(this.comboBox_AJ);
            this.Controls.Add(this.comboBox_ADL);
            this.Controls.Add(this.label_ActionJump);
            this.Controls.Add(this.label_ActionDiagonalRight);
            this.Controls.Add(this.label_ActionDiagonalLeft);
            this.Controls.Add(this.label_ActionModifier);
            this.Controls.Add(this.label_ActionBackward);
            this.Controls.Add(this.label_ActionForward);
            this.Controls.Add(this.label_ActionRight);
            this.Controls.Add(this.label_ActionLeft);
            this.Controls.Add(this.numericUpDown_AR);
            this.Controls.Add(this.numericUpDown_AL);
            this.Controls.Add(this.comboBox_AM);
            this.Controls.Add(this.comboBox_AF);
            this.Controls.Add(this.comboBox_AB);
            this.Controls.Add(this.comboBox_AR);
            this.Controls.Add(this.comboBox_AL);
            this.Name = "BoardActionKeyboardConfig";
            this.Size = new System.Drawing.Size(280, 219);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ADR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ADL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_AJ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_AM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_AB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_AF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_AR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_AL)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown numericUpDown_ADR;
        private System.Windows.Forms.NumericUpDown numericUpDown_ADL;
        private System.Windows.Forms.NumericUpDown numericUpDown_AJ;
        private System.Windows.Forms.NumericUpDown numericUpDown_AM;
        private System.Windows.Forms.NumericUpDown numericUpDown_AB;
        private System.Windows.Forms.NumericUpDown numericUpDown_AF;
        private System.Windows.Forms.ComboBox comboBox_ADR;
        private System.Windows.Forms.ComboBox comboBox_AJ;
        private System.Windows.Forms.ComboBox comboBox_ADL;
        private System.Windows.Forms.Label label_ActionJump;
        private System.Windows.Forms.Label label_ActionDiagonalRight;
        private System.Windows.Forms.Label label_ActionDiagonalLeft;
        private System.Windows.Forms.Label label_ActionModifier;
        private System.Windows.Forms.Label label_ActionBackward;
        private System.Windows.Forms.Label label_ActionForward;
        private System.Windows.Forms.Label label_ActionRight;
        private System.Windows.Forms.Label label_ActionLeft;
        private System.Windows.Forms.NumericUpDown numericUpDown_AR;
        private System.Windows.Forms.NumericUpDown numericUpDown_AL;
        private System.Windows.Forms.ComboBox comboBox_AM;
        private System.Windows.Forms.ComboBox comboBox_AF;
        private System.Windows.Forms.ComboBox comboBox_AB;
        private System.Windows.Forms.ComboBox comboBox_AR;
        private System.Windows.Forms.ComboBox comboBox_AL;
    }
}
