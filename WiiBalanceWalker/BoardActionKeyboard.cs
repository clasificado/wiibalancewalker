﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WiiBalanceWalker
{
    class BoardActionKeyboard : IBoardAction
    {
        public string ConfigControlTitle
        {
            get { return "Keyboard"; }
        }

        public string ConfigControlTypeName
        {
            get { return "WiiBalanceWalker.BoardActionKeyboardConfig"; }
        }

        public System.Windows.Forms.Control ConfigControl
        {
            get;
            set;
        }

        public void Init()
        {
            // Link up form controls with action settings.
            actionList.Left = new ActionItem("Left", (ComboBox)ConfigControl.Controls.Find("comboBox_AL", true)[0], (NumericUpDown)ConfigControl.Controls.Find("numericUpDown_AL", true)[0]);
            actionList.Right = new ActionItem("Right", (ComboBox)ConfigControl.Controls.Find("comboBox_AR", true)[0], (NumericUpDown)ConfigControl.Controls.Find("numericUpDown_AR", true)[0]);
            actionList.Forward = new ActionItem("Forward", (ComboBox)ConfigControl.Controls.Find("comboBox_AF", true)[0], (NumericUpDown)ConfigControl.Controls.Find("numericUpDown_AF", true)[0]);
            actionList.Backward = new ActionItem("Backward", (ComboBox)ConfigControl.Controls.Find("comboBox_AB", true)[0], (NumericUpDown)ConfigControl.Controls.Find("numericUpDown_AB", true)[0]);
            actionList.Modifier = new ActionItem("Modifier", (ComboBox)ConfigControl.Controls.Find("comboBox_AM", true)[0], (NumericUpDown)ConfigControl.Controls.Find("numericUpDown_AM", true)[0]);
            actionList.Jump = new ActionItem("Jump", (ComboBox)ConfigControl.Controls.Find("comboBox_AJ", true)[0], (NumericUpDown)ConfigControl.Controls.Find("numericUpDown_AJ", true)[0]);
            actionList.DiagonalLeft = new ActionItem("DiagonalLeft", (ComboBox)ConfigControl.Controls.Find("comboBox_ADL", true)[0], (NumericUpDown)ConfigControl.Controls.Find("numericUpDown_ADL", true)[0]);
            actionList.DiagonalRight = new ActionItem("DiagonalRight", (ComboBox)ConfigControl.Controls.Find("comboBox_ADR", true)[0], (NumericUpDown)ConfigControl.Controls.Find("numericUpDown_ADR", true)[0]);
        }

        ActionList actionList = new ActionList();
        public BalanceBoardManager.TickData ActionTick(BalanceBoardManager.TickData send)
        {
            
            if ((send.XInputData.GamepadData & XInputData.GamepadEnum.XINPUT_GAMEPAD_DPAD_LEFT) != 0) actionList.Left.Start(); else actionList.Left.Stop();
            if ((send.XInputData.GamepadData & XInputData.GamepadEnum.XINPUT_GAMEPAD_DPAD_RIGHT) != 0) actionList.Right.Start(); else actionList.Right.Stop();
            if ((send.XInputData.GamepadData & XInputData.GamepadEnum.XINPUT_GAMEPAD_DPAD_UP) != 0) actionList.Forward.Start(); else actionList.Forward.Stop();
            if ((send.XInputData.GamepadData & XInputData.GamepadEnum.XINPUT_GAMEPAD_DPAD_DOWN) != 0) actionList.Backward.Start(); else actionList.Backward.Stop();
            if ((send.XInputData.GamepadData & XInputData.GamepadEnum.XINPUT_GAMEPAD_B) != 0) actionList.Modifier.Start(); else actionList.Modifier.Stop();
            if ((send.XInputData.GamepadData & XInputData.GamepadEnum.XINPUT_GAMEPAD_A) != 0) actionList.Jump.Start(); else actionList.Jump.Stop();
            if ((send.XInputData.GamepadData & XInputData.GamepadEnum.XINPUT_GAMEPAD_X) != 0) actionList.DiagonalLeft.Start(); else actionList.DiagonalLeft.Stop();
            if ((send.XInputData.GamepadData & XInputData.GamepadEnum.XINPUT_GAMEPAD_Y) != 0) actionList.DiagonalRight.Start(); else actionList.DiagonalRight.Stop();

            return send;
        }

        public void Stop()
        {
            actionList.Left.Stop();
            actionList.Right.Stop();
            actionList.Forward.Stop();
            actionList.Backward.Stop();
            actionList.Modifier.Stop();
            actionList.Jump.Stop();
            actionList.DiagonalLeft.Stop();
            actionList.DiagonalRight.Stop();
        }
    }
}