﻿//----------------------------------------------------------------------------------------------------------------------+
// WiiBalanceWalker - Released by Richard Perry from GreyCube.com - Under the Microsoft Public License.
//
// Project platform set as x86 for the joystick option work as VJoy.DLL only available as native 32-bit.
//
// Uses the WiimoteLib DLL:           http://wiimotelib.codeplex.com/
// Uses the 32Feet.NET bluetooth DLL: http://32feet.codeplex.com/
// Used the VJoy joystick DLL:        http://headsoft.com.au/index.php?category=vjoy
//----------------------------------------------------------------------------------------------------------------------+

using Ninject;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Timers;
using System.Windows.Forms;
using WiimoteLib;

namespace WiiBalanceWalker
{
    public partial class FormMain : Form
    {
        BalanceBoardManager BalanceBoard = null;
        
        public FormMain()
        {
            InitializeComponent();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            IKernel kernel = new StandardKernel();
            kernel.Load("BoardAction.xml");
            BalanceBoard = kernel.Get<BalanceBoardManager>();
            BalanceBoard.StatusTick += BalanceBoard_StatusTick;

            

            tcAction.TabPages.Clear();

            foreach (var bak in BalanceBoard.BoardAction)
            {
                var ba = bak.Value;
                var t = new TabPage(ba.ConfigControlTitle);
                var p = new Panel();
                p.AutoScroll = true;
                t.Controls.Add(p);
                p.Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top | AnchorStyles.Bottom;
                tcAction.TabPages.Add(t);
                var ConfigControlName = ba.ConfigControlTypeName;
                var c = (Control)Activator.CreateInstance(Type.GetType(ConfigControlName));
                p.Controls.Add(c);

                ba.ConfigControl = c;
                ba.Init();
            }
        }

        void BalanceBoard_StatusTick(BalanceBoardManager.StatusData obj)
        {

            label_Status.Text = obj.status;
            label_rwWT.Text = obj.RawWeightTotal.ToString("0.0");
            label_rwTL.Text = obj.RawWeightTopLeft.ToString("0.0");
            label_rwTR.Text = obj.RawWeightTopRight.ToString("0.0");
            label_rwBL.Text = obj.RawWeightBottomLeft.ToString("0.0");
            label_rwBR.Text = obj.RawWeightBottomRight.ToString("0.0");

            label_owWT.Text = obj.OffsetWeightTotal.ToString("0.0");
            label_owTL.Text = obj.OffsetWeightTopLeft;
            label_owTR.Text = obj.OffsetWeightTopRight;
            label_owBL.Text = obj.OffsetWeightBottomLeft;
            label_owBR.Text = obj.OffsetWeightBottomRight;

            label_owrTL.Text = obj.OffsetWeightRatioTopLeft;
            label_owrTR.Text = obj.OffsetWeightRatioTopRight;
            label_owrBL.Text = obj.OffsetWeightRatioBottomLeft;
            label_owrBR.Text = obj.OffsetWeightRatioBottomRight;

            label_brX.Text = obj.BalanceRatioX.ToString("0,0");
            label_brY.Text = obj.BalanceRatioY.ToString("0,0");
            label_brDL.Text = obj.BalanceRatioDiagonalLeft;
            label_brDR.Text = obj.BalanceRatioDiagonalRight;
            label_brDF.Text = obj.BalanceRatioDF;
        }

        private void button_ResetDefaults_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Reset();
        }

        private void button_BluetoothAddDevice_Click(object sender, EventArgs e)
        {
            var form = new FormBluetooth();
            form.ShowDialog(this);
        }

        private void button_Connect_Click(object sender, EventArgs e)
        {
            try
            {
                this.BalanceBoard.Connect();
                
                // Prevent connect being pressed more than once.

                button_Connect.Enabled = false;
                button_BluetoothAddDevice.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            BalanceBoard.Disconnect();
        }

        private void checkBox_DisableActions_CheckedChanged(object sender, EventArgs e)
        {
            bool Out = ((CheckBox)sender).Checked;
            BalanceBoard.DisableActions = Out;
        }
    }
}
