﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using WiimoteLib;

namespace WiiBalanceWalker
{
    class BalanceBoardManager
    {
        Wiimote wiiDevice = new Wiimote();
        public SortedList<int, IBoardAction> BoardAction = new SortedList<int, IBoardAction>();

        Task infoUpdateTask = null;
        CancellationTokenSource infoUpdateCancellationTokenSource;
        public bool DisableActions = false;

        //public event Action<TickData> ActionTick;
        public event Action<StatusData> StatusTick;

        public int LeftRightBalanceRatioTrigger { get; set; }
        public int TopDownBalanceRatioTrigger { get; set; }
        public int ModifierLeftRightBalanceRatioTrigger { get; set; }
        public int ModifierTopDownBalanceRatioTrigger { get; set; }

        float naCorners = 0f;
        float oaTopLeft = 0f;
        float oaTopRight = 0f;
        float oaBottomLeft = 0f;
        float oaBottomRight = 0f;

        public BalanceBoardManager() { }
        public BalanceBoardManager(IBoardAction[] boardAction)
        {
            foreach (var o in boardAction)
            {
                AddBoardAction(o);
            }
        }

        private void AddBoardAction(IBoardAction o)
        {
            this.BoardAction.Add(this.BoardAction.Keys.Count, o);
            //this.ActionTick += o.ActionTick;
        }

        public void Connect()
        {
            // Find all connected Wii devices.
            var deviceCollection = new WiimoteCollection();
            deviceCollection.FindAllWiimotes();

            for (int i = 0; i < deviceCollection.Count; i++)
            {
                wiiDevice = deviceCollection[i];

                // Device type can only be found after connection, so prompt for multiple devices.

                if (deviceCollection.Count > 1)
                {
                    var devicePathId = new Regex("e_pid&.*?&(.*?)&").Match(wiiDevice.HIDDevicePath).Groups[1].Value.ToUpper();

                    var err = "Multiple Wii Devices Found";
                    throw new ApplicationException(err);
                }

                // Setup update handlers.

                wiiDevice.WiimoteChanged += wiiDevice_WiimoteChanged;
                wiiDevice.WiimoteExtensionChanged += wiiDevice_WiimoteExtensionChanged;

                // Connect and send a request to verify it worked.

                wiiDevice.Connect();
                wiiDevice.SetReportType(InputReport.IRAccel, false); // FALSE = DEVICE ONLY SENDS UPDATES WHEN VALUES CHANGE!
                wiiDevice.SetLEDs(true, false, false, false);

                // Enable processing of updates.

                infoUpdateCancellationTokenSource = new CancellationTokenSource();
                CancellationToken cancelToken = infoUpdateCancellationTokenSource.Token;
                infoUpdateTask = PeriodicTask.Run(new Action(() => InfoUpdate()), TimeSpan.FromMilliseconds(50), cancelToken);

                break;
            }
        }
        private void wiiDevice_WiimoteChanged(object sender, WiimoteChangedEventArgs e)
        {
            // Called every time there is a sensor update, values available using e.WiimoteState.
            // Use this for tracking and filtering rapid accelerometer and gyroscope sensor data.
            // The balance board values are basic, so can be accessed directly only when needed.
        }

        private void wiiDevice_WiimoteExtensionChanged(object sender, WiimoteExtensionChangedEventArgs e)
        {
            // This is not needed for balance boards.
        }


        private void InfoUpdate()
        {
            var Out = new StatusData();
            var WiimoteState = wiiDevice.WiimoteState;

            if (wiiDevice.WiimoteState.ExtensionType != ExtensionType.BalanceBoard)
            {
                //label_Status.Text
                Out.status = "DEVICE IS NOT A BALANCE BOARD...";
                if (StatusTick != null)
                {
                    StatusTick(Out);
                }
                return;
            }

            // Send actions.
            if (!this.DisableActions)
            {
                var x = new XInputData();
                /*
                if (sendLeft) x.GamepadData = x.GamepadData | XInputData.GamepadEnum.XINPUT_GAMEPAD_DPAD_LEFT;
                if (sendRight) x.GamepadData = x.GamepadData | XInputData.GamepadEnum.XINPUT_GAMEPAD_DPAD_RIGHT;
                if (sendForward) x.GamepadData = x.GamepadData | XInputData.GamepadEnum.XINPUT_GAMEPAD_DPAD_UP;
                if (sendBackward) x.GamepadData = x.GamepadData | XInputData.GamepadEnum.XINPUT_GAMEPAD_DPAD_DOWN;
                if (sendModifier) x.GamepadData = x.GamepadData | XInputData.GamepadEnum.XINPUT_GAMEPAD_B;
                if (sendJump) x.GamepadData = x.GamepadData | XInputData.GamepadEnum.XINPUT_GAMEPAD_A;
                if (sendDiagonalLeft) x.GamepadData = x.GamepadData | XInputData.GamepadEnum.XINPUT_GAMEPAD_X;
                if (sendDiagonalRight) x.GamepadData = x.GamepadData | XInputData.GamepadEnum.XINPUT_GAMEPAD_Y;
                */
                var td = new TickData() { XInputData = x, WiimoteState = WiimoteState, StatusData = Out};
                foreach (var bak in BoardAction)
                {
                    var ba = bak.Value;
                    td = ba.ActionTick(td);
                }
            }

            // Update Status
            if (StatusTick != null)
            {
                StatusTick(Out);
            }
        }
        public class TickData
        {
            public XInputData XInputData;
            public WiimoteState WiimoteState;
            public StatusData StatusData;
        }

        public class StatusData
        {
            public short RawValueTopLeft, RawValueTopRight, RawValueBottomLeft, RawValueBottomRight;
            public float RawWeightTotal, RawWeightTopLeft, RawWeightTopRight, RawWeightBottomLeft, RawWeightBottomRight
                , BalanceRatioX, BalanceRatioY
                , OffsetWeightTotal;

            public string status
                , OffsetWeightTopLeft, OffsetWeightTopRight, OffsetWeightBottomLeft, OffsetWeightBottomRight
                , OffsetWeightRatioTopLeft, OffsetWeightRatioTopRight, OffsetWeightRatioBottomLeft, OffsetWeightRatioBottomRight
                , BalanceRatioDiagonalLeft, BalanceRatioDiagonalRight, BalanceRatioDF;
        }

        internal void Disconnect()
        {
            if (infoUpdateCancellationTokenSource != null)
            {
                infoUpdateCancellationTokenSource.Cancel(true);
            }
            wiiDevice.Disconnect();

            foreach (var bak in this.BoardAction)
            {
                var ba = bak.Value;
                ba.Stop();
            }
        }
    }
}